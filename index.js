// This file is used to register all your cloud functions in GCP.
// DO NOT EDIT/DELETE THIS, UNLESS YOU KNOW WHAT YOU ARE DOING.

exports.samplepythonfunction = require("./samplepython/function.js").handler;
exports.samplepythonfirstpython = require("./samplepython/firstpython.py").handler;
exports.samplepythonhirutest = require("./samplepython/hirutest.py").handler;